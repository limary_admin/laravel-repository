<?php
namespace Sinta\LRepository\Transformer;

use League\Fractal\TransformerAbstract;
use Sinta\LRepository\Contracts\Transformable;

class ModelTransformer extends TransformerAbstract
{
    public function transform(Transformable $model)
    {
        return $model->transform();
    }
}