<?php
namespace Sinta\LRepository\Listeners;

use Illuminate\Support\ServiceProvider;

class LumenRepositoryServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function register()
    {
        $this->commands('Sinta\LRepository\Generators\Commands\RepositoryCommand');
        $this->app->register('Sinta\LRepository\Providers\EventServiceProvider');
    }

    public function provides()
    {
        return [];
    }

}