<?php
namespace Sinta\LRepository\Listeners;

use Illuminate\Support\ServiceProvider;

/**
 * 事件服务
 *
 * Class EventServiceProvider
 * @package Sinta\LRepository\Listeners
 */
class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        'Sinta\LRepository\Events\RepositoryEntityCreated' => [
            'Sinta\LRepository\Listeners\CleanCacheRepository'
        ],
        'Sinta\LRepository\Events\RepositoryEntityUpdated' => [
            'Sinta\LRepository\Listeners\CleanCacheRepository'
        ],
        'Sinta\LRepository\Events\RepositoryEntityDeleted' => [
            'Sinta\LRepository\Listeners\CleanCacheRepository'
        ]
    ];

    public function boot()
    {
        $events = app('events');
        foreach ($this->listen as $event => $listeners) {
            foreach ($listeners as $listener) {
                $events->listen($event, $listener);
            }
        }
    }

    public function register()
    {
        //
    }


    public function listens()
    {
        return $this->listen;
    }

}