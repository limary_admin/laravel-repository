<?php
namespace Sinta\LRepository\Listeners;

use Illuminate\Support\ServiceProvider;



class RepositoryServiceProvider extends ServiceProvider
{
    protected $defer = false;


    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../resources/config/repository.php' => config_path('repository.php')
        ]);
        $this->mergeConfigFrom(__DIR__ . '/../../resources/config/repository.php', 'repository');
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'repository');
    }

    public function register()
    {
        $this->commands('Sinta\LRepository\Generators\Commands\RepositoryCommand');
        $this->commands('Sinta\LRepository\Generators\Commands\TransformerCommand');
        $this->commands('Sinta\LRepository\Generators\Commands\PresenterCommand');
        $this->commands('Sinta\LRepository\Generators\Commands\EntityCommand');
        $this->commands('Sinta\LRepository\Generators\Commands\ValidatorCommand');
        $this->commands('Sinta\LRepository\Generators\Commands\ControllerCommand');
        $this->commands('Sinta\LRepository\Generators\Commands\BindingsCommand');
        $this->commands('Sinta\LRepository\Generators\Commands\CriteriaCommand');
        $this->app->register('Sinta\LRepository\Providers\EventServiceProvider');
    }


    public function provides()
    {
        return [];
    }
}