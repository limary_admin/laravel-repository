<?php
namespace Sinta\LRepository\Traits;

use Illuminate\Contracts\Cache\Repository as CacheRepository;

use Sinta\LRepository\Contracts\CriteriaInterface;
use Sinta\LRepository\Helpers\CacheKeys;
use ReflectionObject;
use Exception;


/**
 *实现可缓存接口
 *
 * Trait CacheableRepository
 * @package Sinta\LRepository\Traits
 */
trait CacheableRepository
{

    protected $cacheRepository = null;

    /**
     * 设置缓存仓储
     *
     * @param CacheRepository $repository
     * @return $this
     */
    public function setCacheRepository(CacheRepository $repository)
    {
        $this->cacheRepository =  $repository;
        return $this;
    }

    /**
     * 获取缓存仓储
     *
     * @return null
     */
    public function getCacheRepository()
    {
        if(is_null($this->cacheRepository)){
            $this->cacheRepository = app(config('repository.cache.repository', 'cache'));
        }
        return $this->cacheRepository;
    }

    /**
     * 是否跳过缓存设置
     *
     * @param bool $status
     * @return $this
     */
    public function skipCache($status = true)
    {
        $this->cacheSkip = $status;
        return $this;
    }

    /**
     * 是否跳过缓存
     *
     * @return bool
     */
    public function isSkippedCache()
    {
        $skipped = isset($this->cacheSkip) ? $this->cacheSkip : false;
        $request = app('Illuminate\Http\Request');
        $skipCacheParam = config('repository.cache.params.skipCache', 'skipCache');
        if ($request->has($skipCacheParam) && $request->get($skipCacheParam)) {
            $skipped = true;
        }
        return $skipped;
    }

    /**
     * 是否开启缓存
     *
     * @param $method
     * @return bool
     */
    protected function allowedCache($method)
    {
        $cacheEnabled = config('repository.cache.enabled', true);
        if (!$cacheEnabled) {
            return false;
        }
        $cacheOnly = isset($this->cacheOnly) ? $this->cacheOnly : config('repository.cache.allowed.only', null);
        $cacheExcept = isset($this->cacheExcept) ? $this->cacheExcept : config('repository.cache.allowed.except', null);
        if (is_array($cacheOnly)) {
            return in_array($method, $cacheOnly);
        }
        if (is_array($cacheExcept)) {
            return !in_array($method, $cacheExcept);
        }
        if (is_null($cacheOnly) && is_null($cacheExcept)) {
            return true;
        }
        return false;
    }


    /**
     * 缓存key
     *
     * @param $method
     * @param null $args
     * @return string
     */
    public function getCacheKey($method, $args = null)
    {
        $request = app('Illuminate\Http\Request');
        $args = serialize($args);
        $criteria = $this->serializeCriteria();
        $key = sprintf('%s@%s-%s', get_called_class(), $method, md5($args . $criteria . $request->fullUrl()));
        CacheKeys::putKey(get_called_class(), $key);
        return $key;
    }


    protected function serializeCriteria()
    {
        try {
            return serialize($this->getCriteria());
        } catch (Exception $e) {
            return serialize($this->getCriteria()->map(function ($criterion) {
                return $this->serializeCriterion($criterion);
            }));
        }
    }


    protected function serializeCriterion($criterion)
    {
        try {
            serialize($criterion);
            return $criterion;
        } catch (Exception $e) {
            if ($e->getMessage() !== "Serialization of 'Closure' is not allowed") {
                throw $e;
            }
            $r = new ReflectionObject($criterion);
            return [
                'hash' => md5((string) $r),
                'properties' => $r->getProperties(),
            ];
        }
    }


    /**
     * 缓存时间
     *
     * @return mixed
     */
    public function getCacheMinutes()
    {
        $cacheMinutes = isset($this->cacheMinutes) ? $this->cacheMinutes : config('repository.cache.minutes', 30);
        return $cacheMinutes;
    }

    /**
     * 所有列
     *
     * @param array $columns
     * @return mixed
     */
    public function all($columns = ['*'])
    {
        if (!$this->allowedCache('all') || $this->isSkippedCache()) {
            return parent::all($columns);
        }
        $key = $this->getCacheKey('all', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($columns) {
            return parent::all($columns);
        });
        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    /**
     * 数据分页
     *
     * @param null $limit
     * @param array $columns
     * @param string $method
     * @return mixed
     */
    public function paginate($limit = null, $columns = ['*'], $method = 'paginate')
    {
        if (!$this->allowedCache('paginate') || $this->isSkippedCache()) {
            return parent::paginate($limit, $columns, $method);
        }

        $key = $this->getCacheKey('paginate', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($limit, $columns, $method) {
            return parent::paginate($limit, $columns, $method);
        });
        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    /**
     * find
     *
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = ['*'])
    {
        if (!$this->allowedCache('find') || $this->isSkippedCache()) {
            return parent::find($id, $columns);
        }
        $key = $this->getCacheKey('find', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($id, $columns) {
            return parent::find($id, $columns);
        });
        $this->resetModel();
        $this->resetScope();
        return $value;
    }

    /**
     * 通过字段
     *
     * @param $field
     * @param null $value
     * @param array $columns
     * @return null
     */
    public function findByField($field, $value = null, $columns = ['*'])
    {
        if (!$this->allowedCache('findByField') || $this->isSkippedCache()) {
            return parent::findByField($field, $value, $columns);
        }

        $key = $this->getCacheKey('findByField', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($field, $value, $columns) {
            return parent::findByField($field, $value, $columns);
        });

        $this->resetModel();
        $this->resetScope();
        return $value;
    }


    public function findWhere(array $where, $columns = ['*'])
    {
        if (!$this->allowedCache('findWhere') || $this->isSkippedCache()) {
            return parent::findWhere($where, $columns);
        }
        $key = $this->getCacheKey('findWhere', func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function () use ($where, $columns) {
            return parent::findWhere($where, $columns);
        });
        $this->resetModel();
        $this->resetScope();
        return $value;
    }


    public function getByCriteria(CriteriaInterface $criteria)
    {
        if(!$this->allowedCache('getByCriteria') || $this->isSkippedCache()){
            return parent::getByCriteria($criteria);
        }

        $key = $this->getCacheKey('getByCriteria',func_get_args());
        $minutes = $this->getCacheMinutes();
        $value = $this->getCacheRepository()->remember($key, $minutes, function() use($criteria){
            return parent::getByCriteria($criteria);
        });

        $this->resetModel();
        $this->resetScope();
        return $value;

    }

}