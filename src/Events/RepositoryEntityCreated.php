<?php
namespace Sinta\LRepository\Events;


class RepositoryEntityCreated extends RepositoryEventBase
{
    protected $action = "created";
}