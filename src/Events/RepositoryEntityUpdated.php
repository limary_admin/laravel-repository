<?php
namespace Sinta\LRepository\Events;


class RepositoryEntityUpdated extends RepositoryEventBase
{
    protected $action = "updated";
}