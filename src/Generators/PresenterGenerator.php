<?php
namespace Sinta\LRepository\Generators;


class PresenterGenerator extends Generator
{
    protected $stub = 'presenter/presenter';


    public function getRootNamespace()
    {
        return parent::getRootNamespace(). parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    public function getPathConfigNode()
    {
        return 'presenters';
    }

    public function getReplacements()
    {
        $transformerGenerator = new TransformerGenerator([
            'name' => $this->name
        ]);
        $transformer = $transformerGenerator->getRootNamespace() . '\\' . $transformerGenerator->getName() . 'Transformer';
        $transformer = str_replace([
            "\\",
            '/'
        ], '\\', $transformer);
        echo $transformer;

        return array_merge(parent::getReplacements(), [
            'transformer' => $transformer
        ]);
    }

    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getName() . 'Presenter.php';
    }

    public function getBasePath()
    {
        return config('repository.generator.basePath', app()->path());
    }
}