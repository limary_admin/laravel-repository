<?php
namespace Sinta\LRepository\Generators\Migrations;

use Illuminate\Contracts\Support\Arrayable;

class SchemaParser implements Arrayable
{
    protected $customAttributes = [
        'remember_token' => 'rememberToken()',
        'soft_delete'    => 'softDeletes()',
    ];

    protected $schema;


    public function __construct($schema = null)
    {
        $this->schema = $schema;
    }

    public function up()
    {
        return $this->render();
    }

    public function render()
    {
        $results = '';
        foreach ($this->toArray() as $column => $attributes) {
            $results .= $this->createField($column, $attributes);
        }

        return $results;
    }

    public function toArray()
    {
        return $this->parse($this->schema);
    }

    public function parse($schema)
    {
        $this->schema = $schema;
        $parsed = [];
        foreach ($this->getSchemas() as $schemaArray) {
            $column = $this->getColumn($schemaArray);
            $attributes = $this->getAttributes($column, $schemaArray);
            $parsed[$column] = $attributes;
        }

        return $parsed;
    }

    public function getSchemas()
    {
        if (is_null($this->schema)) {
            return [];
        }

        return explode(',', str_replace(' ', '', $this->schema));
    }

    public function getColumn($schema)
    {
        return array_first(explode(':', $schema), function ($key, $value) {
            return $value;
        });
    }


    public function getAttributes($column, $schema)
    {
        $fields = str_replace($column . ':', '', $schema);

        return $this->hasCustomAttribute($column) ? $this->getCustomAttribute($column) : explode(':', $fields);
    }

    public function hasCustomAttribute($column)
    {
        return array_key_exists($column, $this->customAttributes);
    }

    public function getCustomAttribute($column)
    {
        return (array)$this->customAttributes[$column];
    }

    public function createField($column, $attributes, $type = 'add')
    {
        $results = "\t\t\t" . '$table';
        foreach ($attributes as $key => $field) {
            $results .= $this->{"{$type}Column"}($key, $field, $column);
        }

        return $results .= ';' . PHP_EOL;
    }

    public function down()
    {
        $results = '';
        foreach ($this->toArray() as $column => $attributes) {
            $results .= $this->createField($column, $attributes, 'remove');
        }

        return $results;
    }

    protected function addColumn($key, $field, $column)
    {
        if ($this->hasCustomAttribute($column)) {
            return '->' . $field;
        }
        if ($key == 0) {
            return '->' . $field . "('" . $column . "')";
        }
        if (str_contains($field, '(')) {
            return '->' . $field;
        }

        return '->' . $field . '()';
    }

    protected function removeColumn($key, $field, $column)
    {
        if ($this->hasCustomAttribute($column)) {
            return '->' . $field;
        }

        return '->dropColumn(' . "'" . $column . "')";
    }


}