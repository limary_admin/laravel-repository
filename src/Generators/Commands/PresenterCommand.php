<?php
namespace Sinta\LRepository\Generators\Commands;


use Illuminate\Console\Command;
use Sinta\LRepository\Generators\FileAlreadyExistsException;
use Sinta\LRepository\Generators\PresenterGenerator;
use Sinta\LRepository\Generators\TransformerGenerator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class PresenterCommand extends Command
{
    protected $name = 'make:presenter';

    protected $description = 'Create a new presenter.';

    protected $type = 'Presenter';

    public function handle()
    {
        $this->laravel->call([$this,'fire'],func_get_args());
    }

    public function fire()
    {
        try {
            (new PresenterGenerator([
                'name'  => $this->argument('name'),
                'force' => $this->option('force'),
            ]))->run();
            $this->info("Presenter created successfully.");

            if (!\File::exists(app()->path() . '/Transformers/' . $this->argument('name') . 'Transformer.php')) {
                if ($this->confirm('Would you like to create a Transformer? [y|N]')) {
                    (new TransformerGenerator([
                        'name'  => $this->argument('name'),
                        'force' => $this->option('force'),
                    ]))->run();
                    $this->info("Transformer created successfully.");
                }
            }
        } catch (FileAlreadyExistsException $e) {
            $this->error($this->type . ' already exists!');

            return false;
        }
    }

    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of model for which the presenter is being generated.',
                null
            ],
        ];
    }

    public function getOptions()
    {
        return [
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ]
        ];
    }
}