<?php
namespace Sinta\LRepository\Generators\Commands;


use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Sinta\LRepository\Generators\FileAlreadyExistsException;
use Sinta\LRepository\Generators\TransformerGenerator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class TransformerCommand extends Command
{
    protected $name = 'make:transformer';

    protected $description = 'Create a new transformer.';

    protected $type = 'Transformer';

    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }


    public function fire()
    {
        try {
            (new TransformerGenerator([
                'name' => $this->argument('name'),
                'force' => $this->option('force'),
            ]))->run();
            $this->info("Transformer created successfully.");
        } catch (FileAlreadyExistsException $e) {
            $this->error($this->type . ' already exists!');

            return false;
        }
    }

    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of model for which the transformer is being generated.',
                null
            ],
        ];
    }

    public function getOptions()
    {
        return [
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ]
        ];
    }
}