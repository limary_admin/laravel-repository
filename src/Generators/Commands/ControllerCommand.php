<?php
namespace Sinta\LRepository\Generators\Commands;


use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Sinta\LRepository\Generators\ControllerGenerator;
use Sinta\LRepository\Generators\FileAlreadyExistsException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ControllerCommand extends Command
{
    protected $name = 'make:resource';

    protected $description = 'Create a new RESTful controller.';

    protected $type = 'Controller';

    public function __construct()
    {
        $this->name = ((float) app()->version() >= 5.5  ? 'make:rest-controller' : 'make:resource');
        parent::__construct();
    }

    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }


    public function fire()
    {
        try {
            // Generate create request for controller
            $this->call('make:request', [
                'name' => $this->argument('name') . 'CreateRequest'
            ]);

            // Generate update request for controller
            $this->call('make:request', [
                'name' => $this->argument('name') . 'UpdateRequest'
            ]);

            (new ControllerGenerator([
                'name' => $this->argument('name'),
                'force' => $this->option('force'),
            ]))->run();

            $this->info($this->type . ' created successfully.');

        } catch (FileAlreadyExistsException $e) {
            $this->error($this->type . ' already exists!');

            return false;
        }
    }

    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of model for which the controller is being generated.',
                null
            ],
        ];
    }

    public function getOptions()
    {
        return [
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ],
        ];
    }


}