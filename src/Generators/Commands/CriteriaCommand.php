<?php
namespace Sinta\LRepository\Generators\Commands;


use Illuminate\Console\Command;
use Sinta\LRepository\Generators\CriteriaGenerator;
use Sinta\LRepository\Generators\FileAlreadyExistsException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;


class CriteriaCommand extends Command
{
    protected $name = 'make:criteria';

    protected $description = 'Create a new criteria.';

    protected $type = 'Criteria';

    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    public function fire()
    {
        try {
            (new CriteriaGenerator([
                'name' => $this->argument('name'),
                'force' => $this->option('force'),
            ]))->run();

            $this->info("Criteria created successfully.");
        } catch (FileAlreadyExistsException $ex) {
            $this->error($this->type . ' already exists!');
            return false;
        }
    }

    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of class being generated.',
                null
            ],
        ];
    }

    public function getOptions()
    {
        return [
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ],
        ];
    }
}