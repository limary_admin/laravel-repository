<?php
namespace Sinta\LRepository\Generators\Commands;


use Illuminate\Console\Command;
use Sinta\LRepository\Generators\FileAlreadyExistsException;
use Sinta\LRepository\Generators\ValidatorGenerator;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ValidatorCommand extends Command
{
    protected $name = 'make:validator';


    protected $description = 'Create a new validator.';


    protected $type = 'Validator';

    public function handle(){
        $this->laravel->call([$this, 'fire'], func_get_args());
    }

    public function fire()
    {
        try{
            (new ValidatorGenerator([
                'name' => $this->argument('name'),
                'rules' => $this->option('rules'),
                'force' => $this->option('force'),
            ]))->run();

            $this->info("Validator created successfully.");
        }catch(FileAlreadyExistsException $e){

            $this->error($this->type . ' already exists!');
            return false;
        }
    }

    public function getArguments()
    {
        return [
            [
                'name',
                InputArgument::REQUIRED,
                'The name of model for which the validator is being generated.',
                null
            ],
        ];
    }


    public function getOptions()
    {
        return [
            [
                'rules',
                null,
                InputOption::VALUE_OPTIONAL,
                'The rules of validation attributes.',
                null
            ],
            [
                'force',
                'f',
                InputOption::VALUE_NONE,
                'Force the creation if file already exists.',
                null
            ],
        ];
    }
}