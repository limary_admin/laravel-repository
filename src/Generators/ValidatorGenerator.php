<?php
namespace Sinta\LRepository\Generators;

use Sinta\LRepository\Generators\Migrations\RulesParser;
use Sinta\LRepository\Generators\Migrations\SchemaParser;

class ValidatorGenerator extends Generator
{
    protected $stub = 'validator/validator';

    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }

    public function getPathConfigNode()
    {
        return 'validators';
    }

    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getName() . 'Validator.php';
    }

    public function getBasePath()
    {
        return config('repository.generator.basePath', app()->path());
    }

    public function getReplacements()
    {

        return array_merge(parent::getReplacements(), [
            'rules' => $this->getRules(),
        ]);
    }

    public function getRules()
    {
        if (!$this->rules) {
            return '[]';
        }
        $results = '[' . PHP_EOL;

        foreach ($this->getSchemaParser()->toArray() as $column => $value) {
            $results .= "\t\t'{$column}'\t=>'\t{$value}'," . PHP_EOL;
        }

        return $results . "\t" . ']';
    }

    public function getSchemaParser()
    {
        return new RulesParser($this->rules);
    }
}