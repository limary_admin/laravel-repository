<?php

namespace Sinta\LRepository\Generators;


class CriteriaGenerator extends Generator
{
    protected $stub = 'criteria/criteria';



    public function getRootNamespace()
    {
        return parent::getRootNamespace() . parent::getConfigGeneratorClassPath($this->getPathConfigNode());
    }


    public function getPathConfigNode()
    {
        return 'criteria';
    }

    public function getPath()
    {
        return $this->getBasePath() . '/' . parent::getConfigGeneratorClassPath($this->getPathConfigNode(), true) . '/' . $this->getName() . 'Criteria.php';
    }

    public function getBasePath()
    {
        return config('repository.generator.basePath', app()->path());
    }
}