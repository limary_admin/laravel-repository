<?php
namespace Sinta\LRepository\Generators;

class Stub
{

    protected static $basePath = null;


    protected $path;

    protected $replaces = [];


    public function __construct($path, array $replaces = [])
    {
        $this->path = $path;
        $this->replaces = $replaces;
    }

    public static function create($path, array $replaces = [])
    {
        return new static($path, $replaces);
    }

    public static function setBasePath($path)
    {
        static::$basePath = $path;
    }


    public function replace(array $replaces = [])
    {
        $this->replaces = $replaces;
        return $this;
    }

    public function getReplaces()
    {
        return $this->replaces;
    }

    public function __toString()
    {
        return $this->render();
    }

    public function render()
    {
        return $this->getContents();
    }

    public function getContents()
    {
        $contents = file_get_contents($this->getPath());
        foreach($this->replaces as $search => $replace){
            $contents = str_replace('$' . strtoupper($search) . '$', $replace, $contents);
        }
        return $contents;
    }

    public function getPath()
    {
        return static::$basePath . $this->path;
    }


    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }
}