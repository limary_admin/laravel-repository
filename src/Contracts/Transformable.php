<?php
namespace Sinta\LRepository\Contracts;

/**
 * 可转换的
 *
 * Interface Transformable
 * @package Sinta\LRepository\Contracts
 */
interface Transformable
{
    public function transform();
}