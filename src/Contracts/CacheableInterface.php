<?php
namespace Sinta\LRepository\Contracts;


use Illuminate\Contracts\Cache\Repository as CacheRepository;

/**
 * 可缓存接口
 *
 * Interface CacheableInterface
 * @package Sinta\LRepository\Contracts
 */
interface CacheableInterface
{
    /**
     * 设置缓存Repository
     *
     * @param CacheRepository $repository
     * @return mixed
     */
    public function setCacheRepository(CacheRepository $repository);

    /**
     * 获取缓存Repository
     *
     * @return mixed
     */
    public function getCacheRepository();

    /**
     * 获取缓存key
     *
     * @param $method
     * @param array $args
     * @return mixed
     */
    public function getCacheKey($method, $args = []);

    /**
     * 获取缓存分
     *
     * @return mixed
     */
    public function getCacheMinutes();


    public function skipCache($status = true);
}