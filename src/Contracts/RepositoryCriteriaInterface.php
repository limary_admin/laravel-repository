<?php
namespace Sinta\LRepository\Contracts;


interface RepositoryCriteriaInterface
{
    public function pushCriterial($criteria);


    public function popCriterial($criteria);


    public function getCriteria();


    public function getByCriteria(CriteriaInterface $criteria);


    public function skipCriteria($status = true);


    public function resetCriteria();
}