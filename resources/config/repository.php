<?php

return [

    'pagination' => [
        'limit' => 15
    ],

    /**
     *    |--------------------------------------------------------------------------
          | Fractal Presenter Config
          |--------------------------------------------------------------------------
     *
     * ArraySerializer
     * DataArraySerializer
     * JsonApiSerializer
     */
    'fractal' => [
        'params' => [
            'include' => 'include'
        ],
        'serializer' => League\Fractal\Serializer\DataArraySerializer::class
    ],
    'cache' => [
        'enabled'    => false,
        'minutes'    => 30,
        'repository' => 'cache',
        'clean'      => [

            /*
              |--------------------------------------------------------------------------
              | Enable clear cache on repository changes
              |--------------------------------------------------------------------------
              |
              */
            'enabled' => true,

            /*
              |--------------------------------------------------------------------------
              | Actions in Repository
              |--------------------------------------------------------------------------
              |
              | create : Clear Cache on create Entry in repository
              | update : Clear Cache on update Entry in repository
              | delete : Clear Cache on delete Entry in repository
              |
              */
            'on'      => [
                'create' => true,
                'update' => true,
                'delete' => true,
            ]
        ],
        'params'     => [
            'skipCache' => 'skipCache'
        ],
        'allowed'    => [
            'only'   => null,
            'except' => null
        ]

    ],
    'criteria'   => [
        'acceptedConditions' => [
            '=',
            'like'
        ],
        'params'             => [
            'search'       => 'search',
            'searchFields' => 'searchFields',
            'filter'       => 'filter',
            'orderBy'      => 'orderBy',
            'sortedBy'     => 'sortedBy',
            'with'         => 'with',
            'searchJoin'   => 'searchJoin'
        ]
    ],
    'generator'  => [
        'basePath'      => app()->path(),
        'rootNamespace' => 'App\\',
        'stubsOverridePath' => app()->path(),
        'paths'         => [
            'models'       => 'Entities',
            'repositories' => 'Repositories',
            'interfaces'   => 'Repositories',
            'transformers' => 'Transformers',
            'presenters'   => 'Presenters',
            'validators'   => 'Validators',
            'controllers'  => 'Http/Controllers',
            'provider'     => 'RepositoryServiceProvider',
            'criteria'     => 'Criteria'
        ]
    ]
];